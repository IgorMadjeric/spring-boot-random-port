package com.example.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.stereotype.Component;
import org.springframework.util.SocketUtils;
import java.util.List;

@Component
public class WebContainerPortCustomizer implements EmbeddedServletContainerCustomizer {

	@Value("#{'${server.portRange:${server.port:0}}'.split('\\.\\.')}")
	private List<Integer> portRange;

	@Override
	public void customize(ConfigurableEmbeddedServletContainer configurableEmbeddedServletContainer) {
		configurableEmbeddedServletContainer.setPort(getAvailablePort());
	}

	public int getAvailablePort() {
		if(portRange.size() == 1){
			return portRange.get(0);
		}
		return SocketUtils.findAvailableTcpPort(portRange.get(0), portRange.get(1));
	}
}
