package com.example.demo;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.AbstractConfigurableEmbeddedServletContainer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = DEFINED_PORT)
@TestPropertySource(properties = "server.portRange=8000..8005")
public class WebContainerPortCustomizerPortRangeIntegrationTest {

	@Autowired
	private AbstractConfigurableEmbeddedServletContainer container;

	@Test
	public void shouldStartApplicationOnPortBetween8000And8005() {
		Assert.assertThat(container.getPort(), Matchers.isOneOf(8000, 8001, 8002, 8003, 8004, 8005));
	}

}