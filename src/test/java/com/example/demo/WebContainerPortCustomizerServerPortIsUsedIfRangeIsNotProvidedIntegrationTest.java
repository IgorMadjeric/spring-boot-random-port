package com.example.demo;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.AbstractConfigurableEmbeddedServletContainer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = DEFINED_PORT)
@TestPropertySource(properties = "server.port=8090")
public class WebContainerPortCustomizerServerPortIsUsedIfRangeIsNotProvidedIntegrationTest {

	@Autowired
	private AbstractConfigurableEmbeddedServletContainer container;

	@Test
	public void shouldStartApplicationOnDefinedPort() {
		Assert.assertThat(container.getPort(), Matchers.is(8090));
	}

}